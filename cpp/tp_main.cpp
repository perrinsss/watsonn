#include "tp_file.h"



int main() {
  // constraints (input polyhedron)
  region_constraints in_region(3);
  map<int, double> ineq_map;

  ineq_map[-1] = 5;
  ineq_map[1] = -1;
  ineq_map[2] = -1;
  ineq_map[3] = -1;
  linear_inequality ineq(ineq_map);
  in_region.add(ineq);

  ineq_map[1] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[2] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[3] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[2] = -1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[1] = -1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[2] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[3] = -1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  in_region.print();


  region_constraints out_region;
  vector<vector<uint32_t>> node_ids_by_layer;
  vector<computation_graph> network = create_layers_from_file("network_files/neural_network_information_5", node_ids_by_layer);

  for (int i = 0; i < network.size(); i++) {
    out_region = output_box(network[i], in_region, node_ids_by_layer[i+1]);
    in_region = out_region;
  }

  out_region.print();

  return 0;
}
