#include "tp_file.h"
#include <ctime>
#include <cmath>
#include <cassert>



// hit-and-run sampler
map<uint32_t, double> hr_sample(region_constraints region, map<uint32_t, double> old_sample, vector<uint32_t> node_ids, int seed) {
  assert(region.get_space_dimension() == old_sample.size());
  assert(region.check(old_sample));

  srand(time(NULL) + seed);
  
  // get random direction vector
  map<uint32_t, double> direction;
  double norm = 0;
    
  for (uint32_t i : node_ids) {
    direction[i] = rand() % 256 - 128;
    norm += pow(direction[i], 2);
  }

  norm = sqrt(norm);
    
  for (uint32_t i : node_ids) {
    direction[i] = direction[i]/norm;
  }
  
  // find the maximum offsets in given direction
  map<uint32_t, double> max_point = old_sample;
  map<uint32_t, double> min_point = old_sample;
  map<uint32_t, double> tmp;
  double offmin = 0;
  double offmax = 0;
  double step = 1;

  while (step > 1e-6) {
    for (uint32_t i : node_ids) {
      tmp[i] = max_point[i] + step*direction[i];
    }
    if (region.check(tmp)) {
      max_point = tmp;
      offmax += step;
    } else {
      step /= 2;
    }
  }

  step = -1;

  while (step < -1e-6) {
    for (uint32_t i : node_ids) {
      tmp[i] = min_point[i] + step*direction[i];
    }
    if (region.check(tmp)) {
      min_point = tmp;
      offmin += step;
    } else {
      step /= 2;
    }
  }

  // create new sample from the old one by adding a random offset in the computed range
  map<uint32_t, double> new_sample;

  double rand_val = rand() % 256;
  double offset = (rand_val/255)*(offmax - offmin) + offmin;

  for (uint32_t i : node_ids) {
    new_sample[i] = old_sample[i] + offset*direction[i];
  }

  return new_sample;
}



// compute the initial output box of a layer
region_constraints output_box(computation_graph cg, region_constraints in_reg, vector<uint32_t> node_ids) {
  double bound;
  sherlock shk(cg);
  map<int, double> ineq_map;
  linear_inequality ineq;
  region_constraints out_reg(node_ids.size());

  for (uint32_t i : node_ids) {
    for (uint32_t j : node_ids) {
      ineq_map[j] = 0;
    }

    // lower bound
    shk.optimize_node(i, false, in_reg, bound);
    ineq_map[-1] = -bound;
    ineq_map[i] = 1;
    ineq.update(ineq_map);
    out_reg.add(ineq);

    // upper bound
    shk.optimize_node(i, true, in_reg, bound);
    ineq_map[-1] = bound;
    ineq_map[i] = -1;
    ineq.update(ineq_map);
    out_reg.add(ineq);
  }

  return out_reg;
}



// sample points from the output region and test if they are good or bad
void sample_and_sort(computation_graph cg, region_constraints in_reg, region_constraints out_reg, int n_samples,
		     vector<map<uint32_t, double>>& good_points, vector<map<uint32_t, double>>& bad_points, vector<uint32_t> node_ids) {
  map<uint32_t, double> sample;

  // assuming that the point (0,0,...,0) is always in the initial output region
  for (uint32_t i : node_ids) {
    sample[i] = 0;
  }

  sherlock shk(cg);

  for (int i = 0; i < n_samples; i++) {
    sample = hr_sample(out_reg, sample, node_ids, i);

    if (shk.check_satisfaction(in_reg, sample)) {
      good_points.push_back(sample);
    } else {
      bad_points.push_back(sample);
    }
  }
}



// linear separator using gurobi
linear_inequality separate(vector<map<uint32_t, double>> good_points, vector<map<uint32_t, double>> bad_points, vector<uint32_t> node_ids) {
  map<int, double> ineq_map;

  // gurobi setup
  GRBEnv * env = new GRBEnv();
  GRBModel * model = new GRBModel(*env);

  // variables
  for (uint32_t i : node_ids) {

  }

  // constraints

  // objective

  linear_inequality separator(ineq_map);
  return separator;
}



// add new direction to output polyhedron and compute bounds
region_constraints add_new_direction(computation_graph cg, region_constraints in_reg,
                                     region_constraints out_reg, vector<uint32_t> node_ids,
                                     vector<double> coefs) {
  assert(node_ids.size() == coefs.size());

  // create temporary node with no activation function (just a linear combination of the output nodes)
  uint32_t tmp_id = node_ids.back() + 1;
  node node_x(tmp_id, "none");
  cg.add_new_node(tmp_id, node_x);
  cg.mark_node_as_output(tmp_id);

  // connect the output nodes to the temporary node with given coefficients
  for (int i = 0; i < node_ids.size(); i++) {
    cg.connect_node1_to_node2_with_weight(node_ids[i], tmp_id, coefs[i]);
  }
  cg.set_bias_of_node(tmp_id, 0);

  // compute the output range of the temporary node
  sherlock shk(cg);
  double tmp_min, tmp_max;
  shk.optimize_node(tmp_id, false, in_reg, tmp_min);
  shk.optimize_node(tmp_id, true, in_reg, tmp_max);

  // add a new output constraint accordingly
  map<int, double> ineq_map;

  ineq_map[-1] = -tmp_min;
  for (int i = 0; i < node_ids.size(); i++) {
    ineq_map[node_ids[i]] = coefs[i];
  }
  linear_inequality ineq(ineq_map);
  out_reg.add(ineq);

  ineq_map[-1] = tmp_max;
  for (int i = 0; i < node_ids.size(); i++) {
    ineq_map[node_ids[i]] = -coefs[i];
  }
  ineq.update(ineq_map);
  out_reg.add(ineq);

  return out_reg;
}



// create a computation graph for each layer of the nn from a text file
vector<computation_graph> create_layers_from_file(string filename,
						  vector<vector<uint32_t>>& node_ids_by_layer) {
  ifstream file;
  file.open(filename.c_str(), ios::in);

  int n_inputs, n_outputs, n_layers;
  vector<uint32_t> network_configuration;
  double buffer;

  // get the number of inputs, outputs, layers, and nodes in each layer
  file >> buffer; n_inputs = (int) buffer;
  file >> buffer; n_outputs = (int) buffer;
  file >> buffer; n_layers = (int) buffer;

  for (int i = 0; i < n_layers; i++) {
    file >> buffer; network_configuration.push_back((uint32_t) buffer);
  }

  vector<computation_graph> network;
  computation_graph current_layer;
  computation_graph next_layer;

  int node_id = 1;
  vector<uint32_t> prev_layer_node_ids, current_layer_node_ids;

  // add input nodes to the first layer
  for (int i = 0; i < n_inputs; i++) {
    node node_x(node_id, "constant");
    current_layer.add_new_node(node_id, node_x);
    current_layer.mark_node_as_input(node_id);

    prev_layer_node_ids.push_back(node_id);
    node_id++;
  }

  node_ids_by_layer.push_back(prev_layer_node_ids);

  int n_nodes_prev_layer = n_inputs;

  // add intermediary nodes as outputs of the current layer and inputs of the next layer
  for (int i = 0; i < n_layers; i++) {
    current_layer_node_ids.clear();
    next_layer.clear();

    for (int j = 0; j < network_configuration[i]; j++) {
      node node_x(node_id, "relu");
      current_layer.add_new_node(node_id, node_x);
      current_layer.mark_node_as_output(node_id);

      // get weights
      for (int k = 0; k < n_nodes_prev_layer; k++) {
	file >> buffer; current_layer.connect_node1_to_node2_with_weight(prev_layer_node_ids[k], node_id, buffer);
      }

      // get bias
      file >> buffer; current_layer.set_bias_of_node(node_id, buffer);

      node node_y(node_id, "constant");
      next_layer.add_new_node(node_id, node_y);
      next_layer.mark_node_as_input(node_id);

      current_layer_node_ids.push_back(node_id);
      node_id++;
    }

    node_ids_by_layer.push_back(current_layer_node_ids);

    n_nodes_prev_layer = network_configuration[i];
    prev_layer_node_ids = current_layer_node_ids;

    network.push_back(current_layer);
    current_layer = next_layer;
  }

  current_layer_node_ids.clear();

  // add output nodes to the last layer
  for (int i = 0; i < n_outputs; i++) {
    node node_x(node_id, "relu");
    current_layer.add_new_node(node_id, node_x);
    current_layer.mark_node_as_output(node_id);

    // get weights
    for (int j = 0; j < n_nodes_prev_layer; j++) {
      file >> buffer; current_layer.connect_node1_to_node2_with_weight(prev_layer_node_ids[j], node_id, buffer);
    }

    // get bias
    file >> buffer; current_layer.set_bias_of_node(node_id, buffer);

    current_layer_node_ids.push_back(node_id);
    node_id++;
  }

  node_ids_by_layer.push_back(current_layer_node_ids);

  network.push_back(current_layer);

  return network;
}
