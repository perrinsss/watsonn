#ifndef _TP_FILE_
#define _TP_FILE_

#include "sherlock.h"
#include <cstdlib>

using namespace std;



// hit-and-run sampler
map<uint32_t, double> hr_sample(region_constraints, map<uint32_t, double>, vector<uint32_t>, int);

// compute the initial output box of a layer
region_constraints output_box(computation_graph, region_constraints, vector<uint32_t>);

// sample points from the output region and test if they are good or bad
void sample_and_sort(computation_graph, region_constraints, region_constraints, int,
		     vector<map<uint32_t, double>>, vector<map<uint32_t, double>>, vector<uint32_t>);

// linear separator
linear_inequality separate(vector<map<uint32_t, double>>, vector<map<uint32_t, double>>, vector<uint32_t>);

// add new direction to output polyhedron and compute bounds
region_constraints add_new_direction(computation_graph, region_constraints, region_constraints, vector<uint32_t>, vector<double>);

// create a computation graph for each layer of the nn from a text file
vector<computation_graph> create_layers_from_file(string filename, vector<vector<uint32_t>>);



#endif
