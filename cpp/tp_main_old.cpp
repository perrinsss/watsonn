#include "tp_file.h"



int main() {
  // create network
  computation_graph CG;
  
  node node_1(1, "constant");
  CG.add_new_node(1, node_1);
  node node_2(2, "constant");
  CG.add_new_node(2, node_2);

  node node_3(3, "relu");
  CG.add_new_node(3, node_3);
  node node_4(4, "relu");
  CG.add_new_node(4, node_4);

  CG.mark_node_as_input(1);
  CG.mark_node_as_input(2);

  CG.mark_node_as_output(3);
  CG.mark_node_as_output(4);

  CG.connect_node1_to_node2_with_weight(1, 3, 1.0);
  CG.connect_node1_to_node2_with_weight(1, 4, -2.0);
  CG.connect_node1_to_node2_with_weight(2, 3, 2.0);
  CG.connect_node1_to_node2_with_weight(2, 4, 1.0);

  CG.set_bias_of_node(3, 1.0);
  CG.set_bias_of_node(4, -1.0);

  sherlock sherlock_handler(CG);

  // list of output nodes' indices
  vector<uint32_t> out_node_ids;
  out_node_ids.push_back(3);
  out_node_ids.push_back(4);
  
  // constraints (input polyhedron)
  region_constraints in_region(2);
  map<int, double> ineq_map;
  
  ineq_map[-1] = 5;
  ineq_map[1] = -1;
  ineq_map[2] = -1;
  linear_inequality ineq(ineq_map);
  in_region.add(ineq);

  ineq_map[1] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[2] = 1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  ineq_map[1] = -1;
  ineq.update(ineq_map);
  in_region.add(ineq);

  in_region.print();

  // compute initial output box
  region_constraints out_region = output_box(CG, in_region, out_node_ids);

  // add new directions to the output polyhedron
  vector<double> coefs;
  coefs.push_back(1);
  coefs.push_back(1);
  out_region = add_new_direction(CG, in_region, out_region, out_node_ids, coefs);

  coefs[1] = -1;
  out_region = add_new_direction(CG, in_region, out_region, out_node_ids, coefs);

  coefs[1] = 2;
  out_region = add_new_direction(CG, in_region, out_region, out_node_ids, coefs);

  out_region.print();

  // sample points from the output region
  vector<map<uint32_t, double>> train_samples;
  map<uint32_t, double> sample;
  sample[3] = 5;
  sample[4] = 5;
  train_samples.push_back(sample);

  bool sat = sherlock_handler.check_satisfaction(in_region, sample);
  cout << "sample n°1: [" << sample[3] << ", " << sample[4] << "] - " << (sat ? "yes" : "no") << endl;

  for (int i = 2; i <= 10; i++) {
    sample = hr_sample(out_region, sample, out_node_ids, i);
    train_samples.push_back(sample);

    sat = sherlock_handler.check_satisfaction(in_region, sample);
    cout << "sample n°" << i << ": [" << sample[3] << ", " << sample[4] << "] - " << (sat ? "yes" : "no") << endl;
  }

//  // create and train the svm  
//  svm_model *classifier = svm_classify(sherlock_handler, in_region, train_samples, out_node_ids);
//
//  // sample new points and test the svm
//  svm_node *svm_sample = (svm_node *)malloc(out_node_ids.size() * sizeof(svm_node));
//
//  for (int i = 11; i <= 20; i++) {
//    sample = hr_sample(out_region, sample, out_node_ids, i);
//
//    sat = sherlock_handler.check_satisfaction(in_region, sample);
//    cout << "sample n°" << i << ": [" << sample[3] << ", " << sample[4] << "] - " << (sat ? "yes" : "no") << endl;
//
//    for (int j = 0; j < out_node_ids.size(); j++) {
//      svm_sample[j].index = out_node_ids[j];
//      svm_sample[j].value = sample[out_node_ids[j]];
//    }
//
//    cout << "class predicted by SVM: " << svm_predict(classifier, svm_sample) << endl;
//  }

//  map<uint32_t, double> test_point;
//  test_point[3] = 11;
//  test_point[4] = 3;
//  sat = sherlock_handler.check_satisfaction(in_region, test_point);
//  cout << "test point: [" << test_point[3] << ", " << test_point[4] << "] - " << (sat ? "yes" : "no") << endl;
 

  return 0;
}
