from __future__ import absolute_import
from __future__ import division

import numpy as np
import random as rd



def create_rectangle(dim, d_min, d_max):
  c_rec = []
  d_rec = []

  for i in range(dim):
    ci1 = np.zeros(dim)
    ci1[i] = 1
    c_rec.append(ci1)

    di1 = d_min + (d_max - d_min) * rd.random()
    d_rec.append(di1)

    ci2 = np.zeros(dim)
    ci2[i] = -1
    c_rec.append(ci2)

    di2 = d_min + (d_max - d_min) * rd.random()
    d_rec.append(di2)

  c_rec = np.array(c_rec)
  d_rec = np.array(d_rec)
  return c_rec, d_rec



def generate_samples_from_rectangle(n_points, dim, c_rec, d_rec):
  x_data = []
  y_data = []

  for i in range(n_points):
    p = []
    p_flag = True

    for j in range(dim):
      pj = 2**(1.1/dim) * (-d_rec[2*j+1] + (d_rec[2*j] + d_rec[2*j+1]) * rd.random())
      p.append(pj)

      if pj > d_rec[2*j] or pj < -d_rec[2*j+1]:
	p_flag = False

    if p_flag:
      x_data.append(p)
    else:
      y_data.append(p)

  x_data = np.array(x_data)
  y_data = np.array(y_data)

  return x_data, y_data
