from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import random as rd
import time

import data_gen
import constraints_init
import results_eval



# parameters
dim = 2
n_edges = 2 * dim
n_points = 1000
batch_size = 25


t0 = time.time()


# generate goal constraints (a rectangle for now)
c_rec, d_rec = data_gen.create_rectangle(dim, 1, 5)

#print("goal weights:\n", c_goal)
#print("goal biases: ", d_goal, "\n")


t1 = time.time()
print("time elapsed for rectangle generation: ", t1 - t0)


# generate samples and classify them into good (inside the goal constraints) or bad
x_data, y_data = data_gen.generate_samples_from_rectangle(n_points, dim, c_rec, d_rec)

print("number of good points: ", len(x_data))
print("number of bad points: ", len(y_data))

# placeholders for inputs
X = tf.compat.v1.placeholder(dtype=tf.float32, name="X", shape=[None, dim])
Y = tf.compat.v1.placeholder(dtype=tf.float32, name="Y", shape=[None, dim])


t2 = time.time()
print("time elapsed for data generation: ", t2 - t1)


# initialize weights
c_init = constraints_init.initialize_weights(dim, n_edges)

C = tf.compat.v1.get_variable("C", dtype=tf.float32, initializer=tf.constant(c_init))
C_n = tf.norm(C, 2, 1)
#C_nbc = tf.broadcast_to(C_n, [batch_size, n_edges])



# initialize biases
x_sample = x_data[int(len(x_data)*rd.random())]

d_init = constraints_init.initialize_biases(dim, n_edges, c_init, x_sample)

d = tf.compat.v1.get_variable("d", dtype=tf.float32, initializer=tf.constant(d_init))
d_bc = tf.broadcast_to(d, [batch_size, n_edges])



# plot initial constraints
#t = np.arange(-1, 5.5, 0.5)
#for i in range(n_edges):
#  u = -(c_init[i][0]/c_init[i][1])*t + d_init[i]/c_init[i][1]
#  plt.plot(t,u)



# graph construction

# each good point must respect all constraints
cx     = tf.matmul(X, C, transpose_b=True) # actually XtC because this requires fewer transpositions later on
cxd_r  = tf.nn.relu(cx - d_bc)
cxd_rs = tf.reduce_sum(cxd_r)

# each bad point must violate at least one constraint
cy      = tf.matmul(Y, C, transpose_b=True)
cyd_r   = tf.nn.relu(d_bc - cy)
cyd_rm  = tf.reduce_min(cyd_r, 1)
cyd_rms = tf.reduce_sum(cyd_rm)

# the sum of squared cosines of planes must be as low as possible
ccs = 0
for i in range(n_edges):
  for k in range(i):
    ccs += tf.square(tf.tensordot(C[i], C[k], 1) / (C_n[i] * C_n[k]))

# loss function
loss = cxd_rs + cyd_rms + ccs

train = tf.compat.v1.train.AdamOptimizer(0.001).minimize(loss)



# session
with tf.compat.v1.Session() as sess:
#  writer = tf.compat.v1.summary.FileWriter("./tf_log", sess.graph)

  sess.run(tf.compat.v1.global_variables_initializer())

#  print("\ninitial weights:\n", sess.run(C)) 
#  print("initial biases: ", sess.run(d))


  t3 = time.time()
  print("time elapsed for variables initialization: ", t3 - t2)


  l = 0
  for j in range(1000):
    for i in range(int(len(x_data)/batch_size)):
      _, l = sess.run([train, loss], feed_dict={X: x_data[i*batch_size : (i+1)*batch_size, :], Y: y_data[i*batch_size : (i+1)*batch_size, :]})

  c_f = sess.run(C)
  d_f = sess.run(d)

#  print("\nfinal weights:\n", c_f)
#  print("final biases: ", d_f)
  print("final loss: ", l)


  t4 = time.time()
  print("time elapsed for training: ", t4 - t3)


  # compute the cost of each final constraint w.r.t. good points
  results_eval.compute_costs(n_edges, dim, c_f, d_f, x_data)

  # compute accuracy for good & bad points
  results_eval.compute_accuracy(n_edges, dim, c_f, d_f, x_data, y_data)

  # display results (when in 2 dimensions)
  if dim == 2:
    results_eval.plot_results(n_edges, dim, c_f, d_f, x_data, y_data)

#  writer.close()

